<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Welcome</title>

<link href="<c:url value="/resources/css/bootstrap.min.css" /> "rel="stylesheet">
<script src="<c:url value="/resources/js/jquery-1.11.1.min.js" />"></script>
<script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
</head>




<body>
	<div class="container">
		<div class="col-md-offset-2 col-md-9">
			<br><h2 class="text-center">HCL Dhanjyoti Bank</h2><br>
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="panel-title">
					<h4>${first_name}'s Savings Account</h4>
					</div>
				</div>
				<div class="panel-body">
				<div align="center">
				<table border="1">
	        	<th>Account Number</th>
	        	<th>Account Balance</th>
	        	<th>Account Status</th>
	        	<th>Account Created Date</th>
	        	<th>Delete Account</th>
	        	
				<c:forEach var="account" items="${listOfAccounts}" varStatus="status">
	        	<tr>
	        		<td>${account.acct_holder_id}</td>
					<td>${account.acct_balance}</td>
					<td>${account.acct_status}</td>
					<td>${account.acct_created_date}</td>
					<td>
						&nbsp;&nbsp;&nbsp;&nbsp; 
						<a href="deleteAccount?acct_id=${account.acct_id}">Delete</a>
					</td>
							
	        	</tr>
				</c:forEach>	        	
			</table>
			<div><br>
			<form>
			<input type="button" onclick="window.location.href = 'createSavingsAcc?user_id=${user_id}';" value="Create"/>
			<input type="button" onclick="window.location.href = 'http://localhost:8080/hcl-dhanjyoti-bank/beneficiariesForm';" value="Add Beneficiaries"/>
			<input type="button" onclick="window.location.href = 'http://localhost:8080/hcl-dhanjyoti-bank/transferForm';" value="Transfer"/>
         	<input type="button" onclick="window.location.href = 'http://localhost:8080/hcl-dhanjyoti-bank/';" value="Home"/>
      </form>
			</div>
			</div>
				</div>
			</div>
		</div>
	</div>
	<div>
	<h4 class="text-center">${add_success}</h4>
	<h4 class="text-center">${transfer_success}</h4>
	</div>
</body>

</html>