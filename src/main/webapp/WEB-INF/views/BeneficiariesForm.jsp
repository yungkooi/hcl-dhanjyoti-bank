<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Registration</title>
<link href="<c:url value="/resources/css/bootstrap.min.css" />"
	rel="stylesheet">
<script src="<c:url value="/resources/js/jquery-1.11.1.min.js" />"></script>
<script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>

<style type="text/css">
.errormsg {
	color: red;
	font-style: italic;
}
</style>

</head>




<body>
	<div class="container">
		<div class="col-md-offset-3 col-md-7">
			<br><h2 class="text-center">HCL Dhanjyoti Bank</h2><br>
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="panel-title">
					<h3>Add Beneficiaries</h3>
					</div>
				</div>
				<div class="panel-body">
					<form:form id="BeneficiariesForm" modelAttribute="beneficiaries"
						action="addBeneficiaries" method="post">

						<br />
						<div class="form-group">
							<label for="beneficiary_nick_name" class="col-md-3 control-label">Beneficiary Nick Name</label>
							<div class="col-md-9">
								<form:input path="beneficiary_nick_name" cssClass="form-control" name="beneficiary_nick_name" id="beneficiary_nick_name" />
								<form:errors path="beneficiary_nick_name" cssClass="errormsg" />
							</div>
						</div>
						<br />
						<br />
						<br />
						<div class="form-group">
							<label for="beneficiary_name" class="col-md-3 control-label">Beneficiary Name</label>
							<div class="col-md-9">
								<form:input path="beneficiary_name" cssClass="form-control" name="beneficiary_name"
									id="beneficiary_name" />
								<form:errors path="beneficiary_name" cssClass="errormsg" />
							</div>
						</div>
						<br />
						<br />
						<br />
						<div class="form-group">
							<label for="beneficiary_acc_no" class="col-md-3 control-label">Beneficiary Acc No</label>
							<div class="col-md-9">
								<form:input path="beneficiary_acc_no" cssClass="form-control"
									name="beneficiary_acc_no" id="beneficiary_acc_no" />
								<form:errors path="beneficiary_acc_no" cssClass="errormsg"/>
							</div>
						</div>
						<br />
						<br />
						

						<div class="form-group">
							<!-- Button -->
							<div class="col-md-offset-3 col-md-9">
								<br/><form:button cssClass="btn btn-primary">Add</form:button>
								<input type="button" style="margin-left:230px;" 
								onclick="window.location.href = 'http://localhost:8080/hcl-dhanjyoti-bank/register';" value="Reset"/>
								<input type="button" style="margin-left:8px;" 
								onclick="window.location.href = 'http://localhost:8080/hcl-dhanjyoti-bank/';" value="Home"/>
							</div>
						</div>
					</form:form>
				</div>
			</div>
		</div>
	</div><br><br><br><br><br>
</body>

</html>