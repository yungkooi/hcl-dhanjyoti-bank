<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Welcome</title>

<link href="<c:url value="/resources/css/bootstrap.min.css" /> "rel="stylesheet">
<script src="<c:url value="/resources/js/jquery-1.11.1.min.js" />"></script>
<script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
</head>




<body>
	<div class="container">
		<div class="col-md-offset-3 col-md-7">
			<br><h2 class="text-center">HCL Dhanjyoti Bank</h2><br>
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="panel-title">
					<h3>Who would you like to transfer to?</h3>
					</div>
				</div>
				<div class="panel-body">
					<form:form id="regForm" modelAttribute="user"
						action="registerProcess" method="post">

						<br />
						<div class="form-group">
							<label for="beneficiaries" class="col-md-3 control-label">Beneficiaries</label>
							<div class="col-md-9">
								<form:input path="beneficiaries" cssClass="form-control" name="beneficiaries" id="beneficiaries" />
								<form:errors path="beneficiaries" cssClass="errormsg" />
							</div>
						</div>
						<br />
						<br />
						<br />
						<div class="form-group">
							<label for="amount" class="col-md-3 control-label">Amount</label>
							<div class="col-md-9">
								<form:input path="amount" cssClass="form-control" name="amount"
									id="amount" />
								<form:errors path="amount" cssClass="errormsg" />
							</div>
						</div>
						<br />
						<br />
						<br />
						<div class="form-group">
							<label for="remarks" class="col-md-3 control-label">Remarks</label>
							<div class="col-md-9">
								<form:password path="remarks" cssClass="form-control"
									name="remarks" id="remarks" />
								<form:errors path="remarks" cssClass="errormsg"/>
							</div>
						</div>
						<br />
						<br />
						

						<div class="form-group">
							<!-- Button -->
							<div class="col-md-offset-3 col-md-9">
								<br/><form:button cssClass="btn btn-primary">Transfer</form:button>
								<input type="button" style="margin-left:230px;" 
								onclick="window.location.href = 'http://localhost:8080/hcl-dhanjyoti-bank/register';" value="Reset"/>
								<input type="button" style="margin-left:8px;" 
								onclick="window.location.href = 'http://localhost:8080/hcl-dhanjyoti-bank/';" value="Home"/>
							</div>
						</div>
					</form:form>
				</div>
			</div>
		</div>
	</div><br><br><br><br><br>
</body>

</html>