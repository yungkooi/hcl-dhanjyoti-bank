<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Contact Manager Home</title>
    </head>
    <body>
    	<div align="center">
	        <h1>Existing users</h1>
	        <h3><a href="login">Login</a></h3>
	        <table border="1">
	        	<th>No</th>
	        	<th>First Name</th>
	        	<th>Last Name</th>
	        	<th>Mobile Number</th>
	        	<th>Email</th>
	        	<th>Delete User</th>
	        	
				<c:forEach var="user" items="${listOfUsers}" varStatus="status">
	        	<tr>
	        		<td>${status.index + 1}</td>
					<td>${user.first_name}</td>
					<td>${user.last_name}</td>
					<td>${user.mobile_number}</td>
					<td>${user.email_id}</td>
					<%-- <td>${contact.email}</td>
					<td>${contact.address}</td> 
					<td>${contact.telephone}</td>--%>
					<td>
						<%-- <a href="editContact?id=${contact.id}">Edit</a>--%>
						&nbsp;&nbsp;&nbsp;&nbsp; 
						<a href="deleteContact?user_id=${user.user_id}">Delete</a>
					</td>
							
	        	</tr>
				</c:forEach>	        	
			</table>
    	</div>
    </body>
</html>
