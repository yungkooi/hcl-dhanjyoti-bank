<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Registration</title>
<link href="<c:url value="/resources/css/bootstrap.min.css" />"
	rel="stylesheet">
<script src="<c:url value="/resources/js/jquery-1.11.1.min.js" />"></script>
<script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>

<style type="text/css">
.errormsg {
	color: red;
	font-style: italic;
}
</style>

</head>

<body>
	<div class="container">
		<div class="col-md-offset-3 col-md-7">
			<br><h2 class="text-center">HCL Dhanjyoti Bank</h2><br>
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="panel-title">
					<h3>Customer Registration</h3>
					</div>
				</div>
				<div class="panel-body">
					<form:form id="regForm" modelAttribute="user"
						action="registerProcess" method="post">

						<br />
						<div class="form-group">
							<label for="first_name" class="col-md-3 control-label">First Name</label>
							<div class="col-md-9">
								<form:input path="first_name" cssClass="form-control" name="first_name" id="first_name" />
								<form:errors path="first_name" cssClass="errormsg" />
							</div>
						</div>
						<br />
						<br />
						<br />
						<div class="form-group">
							<label for="last_name" class="col-md-3 control-label">Last Name</label>
							<div class="col-md-9">
								<form:input path="last_name" cssClass="form-control" name="last_name" id="last_name" />
								<form:errors path="last_name" cssClass="errormsg" />
							</div>
						</div>
						<br />
						<br />
						<br />
						<div class="form-group">
							<label for="dob" class="col-md-3 control-label">Date of
								Birth</label>
							<div class="col-md-9">
								<form:input path="dob" cssClass="form-control" name="dob"
									id="dob" placeholder="YYYY-MM-DD" />
								<form:errors path="dob" cssClass="errormsg" />
							</div>
						</div>
						<br />
						<br />
						<br />
						<div class="form-group">
							<label for="add_line1" class="col-md-3 control-label">Address
								Line 1</label>
							<div class="col-md-9">
								<form:input path="add_line1" cssClass="form-control"
									name="add_line1" id="add_line1" />
								<form:errors path="add_line1" cssClass="errormsg" />
							</div>
						</div>
						<br />
						<br />
						<br />
						<div class="form-group">
							<label for="add_line2" class="col-md-3 control-label">Address
								Line 2</label>
							<div class="col-md-9">
								<form:input path="add_line2" cssClass="form-control"
									name="add_line2" id="add_line2" />
								<form:errors path="add_line2" cssClass="errormsg" />
							</div>
						</div>
						<br />
						<br />
						<br />
						<div class="form-group">
							<label for="city" class="col-md-3 control-label">City</label>
							<div class="col-md-9">
								<form:input path="city" cssClass="form-control" name="city"
									id="city" />
								<form:errors path="city" cssClass="errormsg" />
							</div>
						</div>
						<br />
						<br />
						<br />
						<div class="form-group">
							<label for="state" class="col-md-3 control-label">State</label>
							<div class="col-md-9">
								<form:input path="state" cssClass="form-control" name="state"
									id="state" />
								<form:errors path="state" cssClass="errormsg" />
							</div>
						</div>
						<br />
						<br />
						<br />
						<div class="form-group">
							<label for="pin" class="col-md-3 control-label">Pin</label>
							<div class="col-md-9">
								<form:input path="pin" cssClass="form-control" name="pin"
									id="pin" />
								<form:errors path="pin" cssClass="errormsg" />
							</div>
						</div>
						<br />
						<br />
						<br />
						<div class="form-group">
							<label for="mobile_number" class="col-md-3 control-label">Mobile
								Number</label>
							<div class="col-md-9">
								<form:input path="mobile_number" cssClass="form-control"
									name="mobile_number" id="mobile_number" />
								<form:errors path="mobile_number" cssClass="errormsg" />
							</div>
						</div>
						<br />
						<br />
						<br />
						<div class="form-group">
							<label for="email_id" class="col-md-3 control-label">Email</label>
							<div class="col-md-9">
								<form:input path="email_id" cssClass="form-control"
									name="email_id" id="email_id" />
								<form:errors path="email_id" cssClass="errormsg" />
							</div>
						</div>
						<br />
						<br />
						<br />
						<div class="form-group">
							<label for="aadhar_id" class="col-md-3 control-label">AADHAR</label>
							<div class="col-md-9">
								<form:input path="aadhar_id" cssClass="form-control"
									name="aadhar_id" id="aadhar_id" />
								<form:errors path="aadhar_id" cssClass="errormsg" />
							</div>
						</div>
						<br />
						<br />
						<br />
						<div class="form-group">
							<label for="pan" class="col-md-3 control-label">Pan</label>
							<div class="col-md-9">
								<form:input path="pan" cssClass="form-control" name="pan"
									id="pan" />
								<form:errors path="pan" cssClass="errormsg" />
							</div>
						</div>
						<br />
						<br />
						<br />
						<div class="form-group">
							<label for="username" class="col-md-3 control-label">Username</label>
							<div class="col-md-9">
								<form:input path="username" cssClass="form-control"
									name="username" id="username" />
								<form:errors path="username" cssClass="errormsg" />
							</div>
						</div>
						<br />
						<br />
						<br />
						<div class="form-group">
							<label for="password" class="col-md-3 control-label">Password</label>
							<div class="col-md-9">
								<form:password path="password" cssClass="form-control"
									name="password" id="password" />
								<form:errors path="password" cssClass="errormsg"/>
							</div>
						</div>
						<br />
						<br />
						

						<div class="form-group">
							<!-- Button -->
							<div class="col-md-offset-3 col-md-9">
								<br/><form:button cssClass="btn btn-primary">Register</form:button>
								<input type="button" style="margin-left:230px;" 
								onclick="window.location.href = 'http://localhost:8080/hcl-dhanjyoti-bank/register';" value="Reset"/>
								<input type="button" style="margin-left:8px;" 
								onclick="window.location.href = 'http://localhost:8080/hcl-dhanjyoti-bank/';" value="Home"/>
							</div>
						</div>
					</form:form>
				</div>
			</div>
		</div>
	</div><br><br><br><br><br>
</body>





</html>