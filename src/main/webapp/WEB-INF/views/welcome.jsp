<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Welcome</title>

<link href="<c:url value="/resources/css/bootstrap.min.css" /> "rel="stylesheet">
<script src="<c:url value="/resources/js/jquery-1.11.1.min.js" />"></script>
<script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>

</head>

<body>
	<div class="container">
		<div class="col-md-offset-2 col-md-7">
			<br><h2 class="text-center">HCL Dhanjyoti Bank</h2><br>
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="panel-title">
					<h4>Welcome ${first_name}</h4>
					</div>
				</div>
				<div class="panel-body">
				<h4>What would you like to do next?</h4>
				<ul>
				<li><h5><a href="createSavingsAcc?user_id=${user_id}">Create Savings account</a></h5></li>
				<li><h5><a href="viewSavingsAcc?user_id=${user_id}">View Savings account</a></h5></li>
				<li><h5><a href="http://localhost:8080/hcl-dhanjyoti-bank/">Return to Homepage</a></h5></li>
				</ul>
				</div>
			</div>
		</div>
	</div>
	
</body>

</html>