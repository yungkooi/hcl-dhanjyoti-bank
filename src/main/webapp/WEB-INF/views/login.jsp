<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Login</title>

<link href="<c:url value="/resources/css/bootstrap.min.css" /> "rel="stylesheet">
<script src="<c:url value="/resources/js/jquery-1.11.1.min.js" />"></script>
<script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>

</head>

<body>
	<div class="container">
		<div class="col-md-offset-3 col-md-7">
			<br><h2 class="text-center">HCL Dhanjyoti Bank</h2><br>
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="panel-title">
					<h3>User Login</h3>
					</div>
				</div>
				<div class="panel-body">
					<form:form id="loginForm" modelAttribute="login" action="loginProcess" cssClass="form-horizontal" method="post">

						<div class="form-group">
							<label for="username" class="col-md-3 control-label">Username</label>
							<div class="col-md-9">
								<form:input path="username" name="username" id="username" cssClass="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label for="password" class="col-md-3 control-label">Password</label>
							<div class="col-md-9">
								<form:input path="password" type="password" name="password" id="password" cssClass="form-control" />
							</div>
						</div>

						<div class="form-group">
							<!-- Button -->
							<div class="col-md-offset-3 col-md-9">
								<form:button id="login" name="login" cssClass="btn btn-primary">Login</form:button>
								<div style="margin-top: 10px;">
								<a href="register"><u>New user? Register here</u></a>
								</div>
							</div>
						</div>
					</form:form>
					<table align="center">
		<tr>
			<td style="color: red;"><b>${message}</b></td>
		</tr>
	</table>
				</div>
			</div>
		</div>
	</div>
</body>





</html>