package com.hcl.model;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class User {
	
	private Integer user_id;
	
	@NotEmpty(message = "Username is mandatory")
	@Size(min = 8, max = 15, message = "Username must be between 8 to 15 characters")
	private String username;
	
	@NotEmpty(message = "Password is mandatory")
	@Size(min = 8, max = 15, message = "Password must be between 8 to 15 characters")
	private String password;
	
	@NotEmpty(message = "First Name is mandatory")
	@Size(max = 100, message = "First Name must be 100 characters or below")
	private String first_name;
	
	@NotEmpty(message = "Last Name is mandatory")
	@Size(max = 100, message = "Last Name must be 100 characters or below")
	private String last_name;
	
	@NotEmpty(message = "Date of Birth is mandatory") //For regex, all dates valid except 31st on alternate date, or Feb 29/30/31. Should use proper DateTime class
	@Pattern(regexp="^(\\d{4})-(0?[1-9]|1[012])-(0?[1-9]|[12][0-9]|3[01])$", message = "Date of Birth format must be YYYY-MM-DD")
	private String dob;
	
	@NotEmpty(message = "Address Line 1 is mandatory")
	@Size(max = 200, message = "Address Line 1 must be 200 characters or below")
	private String add_line1;
	
	@Size(max = 200, message = "Address Line 2 must be 200 characters or below")
	private String add_line2;
	
	@NotEmpty(message = "City is mandatory")
	@Size(max = 50, message = "City must be 50 characters or below")
	private String city;
	
	@NotEmpty(message = "State is mandatory")
	@Size(max = 50, message = "State must be 50 characters or below")
	private String state;
	
	@Size(min=6, max=6, message = "Pin must be equal to 6 digits")
	@Pattern(regexp="(^[0-9]{6})", message = "Pin must be numeric value only")
	private String pin;
	
	@Size(min=10, max=10, message = "Mobile Number must be equal to 10 digits")
	@Pattern(regexp="(^[0-9]{10})", message = "Mobile Number must be numeric value only")
	private String mobile_number;
	
	@NotEmpty(message = "Email is mandatory")
	@Size(max = 50, message = "Email must be 50 characters or below")
	@Email(message = "Email must be in a valid format")
	private String email_id;
	
	@Size(min=16, max=16, message = "AADHAR must be equal to 16 digits")
	@Pattern(regexp="(^[0-9]{16})", message = "AADHAR must be numeric value only")
	private String aadhar_id;
	
	@NotEmpty(message = "Pan is mandatory")
	@Size(min = 10, max = 10, message = "Pan must be equal to 10 characters")
	private String pan;
	
	public Integer getUser_id() {
		return user_id;
	}
	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFirst_name() {
		return first_name;
	}
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	public String getLast_name() {
		return last_name;
	}
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getAdd_line1() {
		return add_line1;
	}
	public void setAdd_line1(String add_line1) {
		this.add_line1 = add_line1;
	}
	public String getAdd_line2() {
		return add_line2;
	}
	public void setAdd_line2(String add_line2) {
		this.add_line2 = add_line2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getPin() {
		return pin;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}
	public String getMobile_number() {
		return mobile_number;
	}
	public void setMobile_number(String mobile_number) {
		this.mobile_number = mobile_number;
	}
	public String getEmail_id() {
		return email_id;
	}
	public void setEmail_id(String email_id) {
		this.email_id = email_id;
	}
	public String getAadhar_id() {
		return aadhar_id;
	}
	public void setAadhar_id(String aadhar_id) {
		this.aadhar_id = aadhar_id;
	}
	public String getPan() {
		return pan;
	}
	public void setPan(String pan) {
		this.pan = pan;
	}

}
