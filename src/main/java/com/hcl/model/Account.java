package com.hcl.model;

public class Account {
	
	private Integer acct_id;
	private Integer acct_holder_id;
	private String acct_balance;
	private String acct_status;
	private String acct_created_date;
	
	public Integer getAcct_id() {
		return acct_id;
	}
	public void setAcct_id(Integer acct_id) {
		this.acct_id = acct_id;
	}
	public Integer getAcct_holder_id() {
		return acct_holder_id;
	}
	public void setAcct_holder_id(Integer acct_holder_id) {
		this.acct_holder_id = acct_holder_id;
	}
	public String getAcct_balance() {
		return acct_balance;
	}
	public void setAcct_balance(String acct_balance) {
		this.acct_balance = acct_balance;
	}
	public String getAcct_status() {
		return acct_status;
	}
	public void setAcct_status(String acct_status) {
		this.acct_status = acct_status;
	}
	public String getAcct_created_date() {
		return acct_created_date;
	}
	public void setAcct_created_date(String acct_created_date) {
		this.acct_created_date = acct_created_date;
	}
	
	

}
