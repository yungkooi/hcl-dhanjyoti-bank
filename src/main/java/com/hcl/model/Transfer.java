package com.hcl.model;

public class Transfer {
	
	private String beneficiary_nickname;
	private int amount;
	private String remarks;
	
	public String getBeneficiary_nickname() {
		return beneficiary_nickname;
	}
	public void setBeneficiary_nickname(String beneficiary_nickname) {
		this.beneficiary_nickname = beneficiary_nickname;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
}
