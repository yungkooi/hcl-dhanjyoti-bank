package com.hcl.model;

public class Beneficiaries {
	
	private String beneficiary_nick_name;
	
	private String beneficiary_name;
	
	private Integer beneficiary_acc_no;
	
	public String getBeneficiary_nick_name() {
		return beneficiary_nick_name;
	}
	public void setBeneficiary_nick_name(String beneficiary_nick_name) {
		this.beneficiary_nick_name = beneficiary_nick_name;
	}
	public String getBeneficiary_name() {
		return beneficiary_name;
	}
	public void setBeneficiary_name(String beneficiary_name) {
		this.beneficiary_name = beneficiary_name;
	}
	public Integer getBeneficiary_acc_no() {
		return beneficiary_acc_no;
	}
	public void setBeneficiary_acc_no(Integer beneficiary_acc_no) {
		this.beneficiary_acc_no = beneficiary_acc_no;
	}
	
}
