package com.hcl.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.hcl.model.Account;
import com.hcl.model.Beneficiaries;
import com.hcl.model.Login;
import com.hcl.model.Transfer;
import com.hcl.model.User;

public class UserDaoImpl implements UserDao {

	JdbcTemplate jdbcTemplate;

	public UserDaoImpl(DataSource dataSource) {
		jdbcTemplate = new JdbcTemplate(dataSource);
	}

	public int register(User user) {
		String sql = "insert into users (username, password, first_name, last_name, dob, "
				+ "add_line1, add_line2, city, state, pin, mobile_number, email_id, aadhar_id, pan) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		return jdbcTemplate.update(sql, new Object[] { user.getUsername(), user.getPassword(),
				user.getFirst_name(), user.getLast_name(), user.getDob(), user.getAdd_line1(), user.getAdd_line2(), user.getCity(),
				user.getState(), user.getPin(), user.getMobile_number(), user.getEmail_id(), user.getAadhar_id(), user.getPan()  });
	}

	public User validateUser(Login login) {
		String sql = "select * from users where username='" + login.getUsername() + "' and password='" + login.getPassword()
		+ "'";
		List<User> users = jdbcTemplate.query(sql, new UserMapper());

		return users.size() > 0 ? users.get(0) : null;
	}

	public List<User> listUsers() {
		String sql = "SELECT * FROM users";
		List<User> listOfUsers = jdbcTemplate.query(sql, new RowMapper<User>() {

			@Override
			public User mapRow(ResultSet rs, int rowNum) throws SQLException {
				User userInfo = new User();

				userInfo.setUser_id(rs.getInt("user_id"));
				userInfo.setFirst_name(rs.getString("first_name"));
				userInfo.setLast_name(rs.getString("last_name"));
				userInfo.setMobile_number(rs.getString("mobile_number"));
				userInfo.setEmail_id(rs.getString("email_id"));

				return userInfo;
			}

		});

		return listOfUsers;
	}

	@Override
	public void delete(int user_Id) {
		String sql = "DELETE FROM users WHERE user_id=?";
		jdbcTemplate.update(sql, user_Id);
	}

	@Override
	public void createSavings(int user_Id) {
		String sql = "INSERT INTO accounts (acct_holder_id, acct_balance, acct_status, acct_created_date) VALUES (?, 10000, \"A\", ?)";
		jdbcTemplate.update(sql, new Object [] {user_Id, getCurrentDate() });

	}

	@Override
	public List<Account> listAccounts(int acct_Holder_Id) {
		String sql = "SELECT * FROM accounts WHERE acct_holder_id=?";
		List<Account> listOfAccounts = jdbcTemplate.query(sql, new Object[] {acct_Holder_Id}, new RowMapper<Account>() {

			public Account mapRow(ResultSet rs, int rowNum) throws SQLException {
				Account accountInfo = new Account();

				accountInfo.setAcct_balance(rs.getString("acct_balance"));
				accountInfo.setAcct_status(rs.getString("acct_status"));
				accountInfo.setAcct_created_date(rs.getString("acct_created_date"));
				accountInfo.setAcct_id(rs.getInt("acct_id"));
				accountInfo.setAcct_holder_id(rs.getInt("acct_holder_id"));

				return accountInfo;
			}

		});

		return listOfAccounts;
	}
	
	@Override
	public void deleteAccount(int acct_Id) {
		String sql = "DELETE FROM accounts WHERE acct_id=?";
		jdbcTemplate.update(sql, acct_Id);
	}
	
	public String getCurrentDate() {
	DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");  
	   LocalDateTime now = LocalDateTime.now();  
	   System.out.println("CURRENT DATE: " + dtf.format(now)); 
	   return dtf.format(now);
	}
	
	public void checkBeneficiaryBelongToWhichAcct(Beneficiaries beneficiaries, Account account, Transfer transfer) {
		
		account.setAcct_holder_id(beneficiaries.getBeneficiary_acc_no());
		List<Account> currentAccList = listAccounts(account.getAcct_holder_id());
		Integer updatedSum = Integer.parseInt(currentAccList.get(0).getAcct_balance()) - transfer.getAmount();
		account.setAcct_balance(updatedSum.toString());
	}


	@Override
	public int addBeneficiaries(Beneficiaries beneficiaries) {
		String sql = "INSERT INTO beneficiaries (beneficiary_nick_name, beneficiary_name, beneficiary_acc_no) VALUES (?, ?, ?)";

		return jdbcTemplate.update(sql, new Object[] { beneficiaries.getBeneficiary_nick_name(), beneficiaries.getBeneficiary_name(), 
				beneficiaries.getBeneficiary_acc_no() });
	}
	
	@Override
	public int transferFunds(String current_acc_bal, int userToTransferTo, Transfer transfer) {
		String sql = "update accounts set acct_balance=? where acct_holder_id=?";
		
		Double updatedSum = Double.parseDouble(current_acc_bal) + transfer.getAmount();
		System.out.println("TRANSFER_UPDATED_SUM: " + updatedSum);
		

		return jdbcTemplate.update(sql, new Object[] { updatedSum, userToTransferTo });
	}
	
	@Override
	public int minusFromCurrentAcc(String current_acc_bal, int userToDeductFrom, Transfer transfer) {
		String sql = "update accounts set acct_balance=? where acct_holder_id=?";
		
		Double updatedSum = Double.parseDouble(current_acc_bal) - transfer.getAmount();
		System.out.println("TRANSFER_DEDUCTED_SUM: " + updatedSum);
		

		return jdbcTemplate.update(sql, new Object[] { updatedSum, userToDeductFrom });
	}
	
}

class UserMapper implements RowMapper<User> {

	public User mapRow(ResultSet rs, int arg1) throws SQLException {
		User user = new User();

		user.setUsername(rs.getString("username"));
		user.setPassword(rs.getString("password"));
		user.setFirst_name(rs.getString("first_name"));
		user.setUser_id(Integer.parseInt(rs.getString("user_id")));

		return user;
	}
}