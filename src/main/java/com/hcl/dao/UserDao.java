package com.hcl.dao;

import java.util.List;

import com.hcl.model.Account;
import com.hcl.model.Beneficiaries;
import com.hcl.model.Login;
import com.hcl.model.Transfer;
import com.hcl.model.User;

public interface UserDao {

  int register(User user);

  User validateUser(Login login);
  
  public List<User> listUsers();
  
  public void delete(int user_Id);
  
  public void createSavings(int user_Id);
  
  public List<Account> listAccounts(int acct_Holder_Id);
  
  public void deleteAccount(int acct_Id);
  
  public int addBeneficiaries(Beneficiaries beneficiaries);
  
  public int transferFunds(String current_acc_bal, int userToTransferTo, Transfer transfer);
  
  public int minusFromCurrentAcc(String current_acc_bal, int userToDeductFrom, Transfer transfer);
}
