package com.hcl.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.hcl.dao.UserDao;
import com.hcl.model.Account;
import com.hcl.model.Beneficiaries;
import com.hcl.model.Login;
import com.hcl.model.Transfer;
import com.hcl.model.User;
import com.hcl.service.UserService;

@Controller
public class LoginController {

	@Autowired
	UserService userService;

	@Autowired
	UserDao userDao;

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView showLogin() {
		ModelAndView mav = new ModelAndView("login");
		mav.addObject("login", new Login());

		return mav;
	}

	@RequestMapping(value = "/loginProcess", method = RequestMethod.POST)
	public ModelAndView loginProcess(@ModelAttribute("login") Login login) {
		ModelAndView mav = null;

		User user = userService.validateUser(login);

		if (null != user) {
			mav = new ModelAndView("welcome");
			System.out.println("user_id: " + user.getUser_id());
			mav.addObject("first_name", user.getFirst_name());
			mav.addObject("user_id", user.getUser_id());
			currentUserId = user.getUser_id();
			currentUserFirstName = user.getFirst_name();

		} else {
			mav = new ModelAndView("login");
			mav.addObject("message", "Username/Password is incorrect!");
		}

		return mav;
	}

	private int currentUserId;
	private String currentUserFirstName;
	private int userToTransferTo;

	@RequestMapping(value = "/createSavingsAcc", method = RequestMethod.GET)
	public ModelAndView createAccProcess(HttpServletRequest request) {


		int userId = Integer.parseInt(request.getParameter("user_id"));
		System.out.println("user id obtained: " + userId);
		userDao.createSavings(userId); 

		ModelAndView model = new ModelAndView();

		List<Account> listOfAccounts = userDao.listAccounts(userId);
		model.addObject("listOfAccounts", listOfAccounts);

		model.addObject("first_name", currentUserFirstName);
		model.addObject("user_id", currentUserId);

		model.setViewName("savings");

		return model;

	}

	@RequestMapping(value = "/deleteAccount", method = RequestMethod.GET)
	public ModelAndView deleteAccount(HttpServletRequest request) {
		int acct_id = Integer.parseInt(request.getParameter("acct_id"));
		userDao.deleteAccount(acct_id);

		ModelAndView model = new ModelAndView();

		model.addObject("first_name", currentUserFirstName);
		model.addObject("user_id", currentUserId);

		List<Account> listOfAccounts = userDao.listAccounts(currentUserId);
		model.addObject("listOfAccounts", listOfAccounts);

		model.setViewName("savings");

		return model;
	}

	@RequestMapping(value = "/viewSavingsAcc", method = RequestMethod.GET)
	public ModelAndView viewAccProcess(HttpServletRequest request) {


		int userId = Integer.parseInt(request.getParameter("user_id"));
		System.out.println("user id obtained: " + userId);

		ModelAndView model = new ModelAndView();

		List<Account> listOfAccounts = userDao.listAccounts(userId);
		model.addObject("listOfAccounts", listOfAccounts);

		model.addObject("first_name", currentUserFirstName);
		model.addObject("user_id", currentUserId);

		model.setViewName("savings");

		return model;

	}

	@RequestMapping(value = "/beneficiariesForm", method = RequestMethod.GET)
	public ModelAndView beneficiaryForm(HttpServletRequest request) {
		ModelAndView mav = new ModelAndView("BeneficiariesForm");
		mav.addObject("beneficiaries", new Beneficiaries());

		return mav;

	}

	@RequestMapping(value = "/addBeneficiaries", method = RequestMethod.POST)
	public ModelAndView addBeneficiaries(@ModelAttribute("beneficiaries") @Valid Beneficiaries beneficiaries, BindingResult result) {

		if (result.hasErrors()) {
			return new ModelAndView("savings");
		} else {
			ModelAndView model = new ModelAndView();
			userDao.addBeneficiaries(beneficiaries);
			userToTransferTo = beneficiaries.getBeneficiary_acc_no();

			List<Account> listOfAccounts = userDao.listAccounts(currentUserId);
			model.addObject("listOfAccounts", listOfAccounts);
			model.addObject("first_name", currentUserFirstName);

			model.addObject("add_success", "Beneficiary added successfully.");
			model.setViewName("savings");
			return model;
		}
	}

	@RequestMapping(value = "/transferForm", method = RequestMethod.GET)
	public ModelAndView transferForm(HttpServletRequest request) {
		ModelAndView mav = new ModelAndView("TransferForm");
		mav.addObject("transfer", new Transfer());
		return mav;

	}

	@RequestMapping(value = "/transferFunds", method = RequestMethod.POST)
	public ModelAndView transfer(@ModelAttribute("transfer") @Valid Transfer transfer, BindingResult result) {

		if (result.hasErrors()) {
			return new ModelAndView("savings");
		} else {
			ModelAndView model = new ModelAndView();
			List<Account> listOfAccounts = userDao.listAccounts(userToTransferTo);
			String current_acc_bal = listOfAccounts.get(0).getAcct_balance();

			System.out.println("TRANSFER_CURRENT_ACC_BAL: " + current_acc_bal);
			System.out.println("TRANSFER_beneficiaryUserId:" + userToTransferTo);

			List<Account> listOfCurrAccountsBeforeDeduction = userDao.listAccounts(currentUserId);
			String current_acc_bal_beforededuction = listOfCurrAccountsBeforeDeduction.get(0).getAcct_balance();
			
			userDao.transferFunds(current_acc_bal, userToTransferTo, transfer);
			userDao.minusFromCurrentAcc(current_acc_bal_beforededuction, currentUserId, transfer);

			List<Account> listOfCurrAccounts = userDao.listAccounts(currentUserId);
			model.addObject("listOfAccounts", listOfCurrAccounts);
			model.addObject("first_name", currentUserFirstName);

			model.addObject("transfer_success", "Funds have been transferred successfully.");
			model.setViewName("savings");
			return model;
		}
	}

}
