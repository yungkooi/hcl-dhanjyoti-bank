package com.hcl.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.hcl.dao.UserDao;
import com.hcl.model.User;

/**
 * This controller routes accesses to the application to the appropriate
 * hanlder methods. 
 * @author www.codejava.net
 *
 */
@Controller
public class HomeController {
	
	@Autowired
	private UserDao userDao;
	
	@RequestMapping(value="/")
	public ModelAndView listContact(ModelAndView model) throws IOException{
		List<User> listOfUsers = userDao.listUsers();
		model.addObject("listOfUsers", listOfUsers);
		model.setViewName("home");
		
		return model;
	}
}
